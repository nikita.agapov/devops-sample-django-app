<h1>Party Parrot App</h1>

<img src='media/images/party-parrot.gif' alt='parrot' height="93" width="124">
<br>
<br>
<h3></h3>

Sample Python application on Django with PostgreSQL database.

<h3>Requirements for local deployment</h3>

____


- docker >= 1:24.0.7-1
- postgresql >= 15.4-2
- docker-compose >= 2.23.3-1

<h3>Deployment</h3>

____


- install postgresql, docker, docker-compose
- Configure postgresql and docker according to your system

`Links for arch-based systems`

[Docker](https://wiki.archlinux.org/title/docker)

[PostgreSQL](https://wiki.archlinux.org/title/PostgreSQL)


- Copy repository
```
git clone https://gitlab.com/nikita.agapov/devops-sample-django-app

```
- Copy .env.example as .env and configure according to your system
```
cp .env.example .env
```
- Build image
```
docker compose build --no-cache    
```
- Start application
```
docker-compose up        
```
- Open application in your browser
```
Default URL is 0.0.0.0:8001
```