#!/bin/sh

echo "Applying database migrations"
python /usr/src/app/manage.py migrate

echo "Starting application"
exec "$@"
